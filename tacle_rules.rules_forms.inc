<?php

/**
 * @file
 * Forms for rules integration with the Taxonomy Access Lite (TACL) module.
 */

/**
 * Form for tacle_rules_action_single_term_role_permission().
 *
 * @param array $settings
 * @param array $form
 */
function tacle_rules_action_single_term_role_permission_form($settings, &$form) {
  $settings += array(
    'tacle_rules' => array(
      'add_permission' => 'grant',
      'schemes' => array(),
      'roles' => array()
    ),
  );
  $form['settings']['tacle_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t(TACLE_RULES_TAC_LITE_NAME),
    '#collapsible' => TRUE,
  );

  _tacle_rules_form_element_permission($settings, $form);
  _tacle_rules_form_element_scheme($settings, $form);

  // Select the role to be changed
  $form['settings']['tacle_rules']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#options' => user_roles(TRUE),
    '#default_value' => $settings['tacle_rules']['roles'],
    '#required' => TRUE,
  );
}

/**
 * Form for tacle_rules_action_multiple_term_role_permission().
 *
 * @param array $settings
 * @param array $form
 */
function tacle_rules_action_multiple_term_role_permission_form($settings, &$form) {
  $settings += array(
    'tacle_rules' => array(
      'add_permission' => 'grant',
      'schemes' => array(),
      'roles' => array(),
      'terms' => array(),
      'terms_select' => array(),
      'terms_id' => '',
    )
  );
  tacle_rules_action_single_term_role_permission_form($settings, $form);
  _tacle_rules_form_element_terms($settings, $form);
}

/**
 * Form validation for tacle_rules_action_multiple_term_role_permission().
 */
function tacle_rules_action_multiple_term_role_permission_validate($form, $form_state) {
  _tacle_rules_multiple_term_validate($form, $form_state);
}

/**
 * Form submit for tacle_rules_action_multiple_term_role_permission().
 */
function tacle_rules_action_multiple_term_role_permission_submit(&$settings, $form, $form_state) {
  _tacle_rules_multiple_term_submit($settings, $form, $form_state);
}

/**
 * Form for tacle_rules_action_single_term_user_permission().
 *
 * @param array $settings
 * @param array $form
 */
function tacle_rules_action_single_term_user_permission_form($settings, &$form) {
  $settings += array(
    'tacle_rules' => array(
      'add_permission' => 'grant',
      'schemes' => array()
    ),
  );
  $form['settings']['tacle_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t(TACLE_RULES_TAC_LITE_NAME),
    '#collapsible' => TRUE,
  );
  _tacle_rules_form_element_permission($settings, $form);
  _tacle_rules_form_element_scheme($settings, $form);
}

/**
 * Form for tacle_rules_action_multiple_term_user_permission().
 *
 * @param array $settings
 * @param array $form
 */
function tacle_rules_action_multiple_term_user_permission_form($settings, &$form) {
  $settings += array(
    'tacle_rules' => array(
      'add_permission' => 'grant',
      'schemes' => array(),
      'terms' => array(),
      'terms_select' => array(),
      'terms_id' => '',
    ),
  );
  tacle_rules_action_single_term_user_permission_form($settings, $form);
  _tacle_rules_form_element_terms($settings, $form);
}

/**
 * Form validation for tacle_rules_action_multiple_term_user_permission().
 */
function tacle_rules_action_multiple_term_user_permission_validate($form, $form_state) {
  _tacle_rules_multiple_term_validate($form, $form_state);
}

/**
 * Form submit for tacle_rules_action_multiple_term_user_permission().
 */
function tacle_rules_action_multiple_term_user_permission_submit(&$settings, $form, $form_state) {
  _tacle_rules_multiple_term_submit($settings, $form, $form_state);
}

function tacle_rules_condition_user_has_tacl_access_form($settings, &$form) {
  $settings += array(
    'tacle_rules' => array(
      'op' => array(),
      'terms' => array(),
      'terms_select' => array(),
      'terms_id' => '',
    ),
  );
  $form['settings']['tacle_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t(TACLE_RULES_TAC_LITE_NAME),
    '#collapsible' => TRUE,
  );
  $form['settings']['tacle_rules']['op'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Access type'),
    '#options' => array(
      'view' => t('View'),
      'update' => t('Update'),
      'delete' => t('Delete'),
    ),
    '#default_value' => $settings['tacle_rules']['op'],
    '#required' => TRUE,
  );
  _tacle_rules_form_element_terms($settings, $form);
}

/**
 * Form validation for tacle_rules_condition_user_has_tacl_access().
 */
function tacle_rules_condition_user_has_tacl_access_validate($form, $form_state) {
  _tacle_rules_multiple_term_validate($form, $form_state);
}

/**
 * Form submit for tacle_rules_condition_user_has_tacl_access().
 */
function tacle_rules_condition_user_has_tacl_access_submit(&$settings, $form, $form_state) {
  _tacle_rules_multiple_term_submit($settings, $form, $form_state);
}

/**
 * Build the schemes input form element.
 *
 * @param array $settings
 * @return array
 */
function _tacle_rules_form_element_scheme($settings, &$form) {
  $scheme_options = _tacle_rules_options_schemes(TRUE);
  $scheme_default = array();
  if (count($scheme_options) == 1) {
    $key = key($scheme_options);
    $scheme_default = array($key => $key);
  }
  if (!empty($settings['tacle_rules']['schemes'])) {
    $scheme_default = $settings['tacle_rules']['schemes'];
  }
  // Select which scheme is to be modified
  $form['settings']['tacle_rules']['schemes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Schemes'),
    '#options' => $scheme_options,
    '#default_value' => $scheme_default,
    '#required' => TRUE,
  );
}

/**
 * Build the permissions form element.
 *
 * @param array $settings
 * @return array
 */
function _tacle_rules_form_element_permission($settings, &$form) {
  $form['settings']['tacle_rules']['add_permission'] = array(
    '#type' => 'radios',
    '#title' => t('Permission'),
    '#options' => array('grant' => t('Grant'), 'revoke' => t('Revoke')),
    '#default_value' => $settings['tacle_rules']['add_permission'],
    '#required' => TRUE,
  );
}

/**
 * Build the multiple term input elements: terms select & terms id.
 *
 * @param array $settings
 * @param array $form
 */
function _tacle_rules_form_element_terms($settings, &$form) {
  $vids = array_keys(_tacle_rules_options_vocabularies(TRUE));
  $term_result = db_query('SELECT tid, vid, name
    FROM {term_data}
    WHERE vid IN (' . db_placeholders($vids) . ')
    ORDER BY name', $vids);
  $term_count = db_affected_rows();

  if ($term_count < 501) {
    $size = 10;
    if ($term_count && $term_count < 10) {
      $size = $term_count;
    }

    $term_options = array();
    while ($term = db_fetch_object($term_result)) {
      $term_options[$term->vid . '|' . $term->tid] = $term->name;
    }

    $form['settings']['tacle_rules']['terms_select'] = array(
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => t('Term select'),
      '#size' => $size,
      '#description' => t('Select terms holding [Ctrl] to select multiple terms.'),
      '#options' => $term_options,
      '#default_value' => $settings['tacle_rules']['terms_select'],
    );

    $terms_id_desc = t('Optional: Enter a comma separated list of term IDs. If entered it will override the select list.');
    $terms_id_req = FALSE;
  }
  else {
    $form['settings']['tacle_rules']['terms_select'] = array(
      '#type' => 'markup',
      '#title' => t('Term select'),
      '#description' => t('More than 500 terms in %tac_lite vocabularies. Select list disabled.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME)),
    );

    $terms_id_desc = t('Enter a comma separated list of term IDs.');
    $terms_id_req = TRUE;
  }

  // @todo: Would be nice as AHAH term name input.
  $form['settings']['tacle_rules']['terms_id'] = array(
    '#type' => 'textarea',
    '#title' => t('Term IDs'),
    '#description' => $terms_id_desc,
    '#default_value' => $settings['tacle_rules']['terms_id'],
    '#required' => $terms_id_req,
  );
}

/**
 * Validate helper function for multiple term input elements terms select &
 * terms id used by role and user functions.
 */
function _tacle_rules_multiple_term_validate($form, $form_state) {
  $settings = $form_state['values']['settings'];
  if (!empty($settings['tacle_rules']['terms_id'])) {
    // @todo: Better regex match pattern.
    if (preg_match('/[^0-9, ]/', $settings['tacle_rules']['terms_id'])) {
      form_set_error('settings][tacle_rules][terms_id', t('Term IDs must be a comma separates list of numeric characters.'));
    }
    else {
      $tids_clean = preg_replace('/[^0-9,]/', '', $settings['tacle_rules']['terms_id']);
      $tids = explode(',', $tids_clean);
      if (count($tids)) {
        $terms = _tacle_rules_multiple_term_load($tids);
        $terms_loaded = array_keys($terms);
        $terms_diff = array_diff($tids, $terms_loaded);
        if (count($terms_diff)) {
          $message = t('The following term IDs are not controlled by %tac_lite: !terms', array(
            '%tac_lite' => TACLE_RULES_TAC_LITE_NAME,
            '!terms' => implode(', ', $terms_diff),
          ));
          form_set_error('settings][tacle_rules][terms_id', $message);
        }
      }
    }
  }
  if (empty($settings['tacle_rules']['terms_select']) && empty($settings['tacle_rules']['terms_id'])) {
    form_set_error('settings][tacle_rules][terms_select', t('Terms must be selected or entered in Term IDs input.'));
  }
}

/**
 * Submit helper function for multiple term input elements terms select &
 * terms id used by role and user functions.
 */
function _tacle_rules_multiple_term_submit(&$settings, $form, $form_state) {
  $fs_tacle = $form_state['values']['settings']['tacle_rules'];
  if (!empty($fs_tacle['terms_id'])) {
    $tids_clean = preg_replace('/[^0-9,]/', '', $fs_tacle['terms_id']);
    $tids = explode(',', $tids_clean);
    if (count($tids)) {
      $terms = _tacle_rules_multiple_term_load($tids);
      if (count($terms)) {
        $settings['tacle_rules']['terms'] = $terms;
        $settings['tacle_rules']['terms_select'] = array();
        $settings['tacle_rules']['terms_id'] = implode(', ', $tids);
      }
    }
  }
  elseif (count($fs_tacle['terms_select'])) {
    $terms = array();
    foreach ($fs_tacle['terms_select'] as $key) {
      $keys = explode('|', $key);
      $term = new stdClass();
      $term->vid = $keys[0];
      $term->tid = $keys[1];
      $term->name = $form['settings']['tacle_rules']['terms_select']['#options'][$key];
      $terms[$term->tid] = $term;
    }
    $settings['tacle_rules']['terms'] = $terms;
  }
}

/**
 * Helper function that loads term using comma separated list of term ids.
 * Limits terms to tac lite controlled vocabularies.
 *
 * @param array $term_ids
 * @return array
 */
function _tacle_rules_multiple_term_load($term_ids) {
  $terms = array();
  if (count($term_ids)) {
    $vids = array_keys(_tacle_rules_options_vocabularies(TRUE));
    $term_result = db_query('SELECT tid, vid, name
      FROM {term_data}
      WHERE tid in (' . db_placeholders($term_ids) . ')
      AND vid in (' . db_placeholders($vids) . ')', array_merge($term_ids, $vids));

    while ($term = db_fetch_object($term_result)) {
      $terms[$term->tid] = $term;
    }
  }
  return $terms;
}

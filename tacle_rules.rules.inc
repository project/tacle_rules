<?php

/**
 * @file
 * Rules integration with the Taxonomy Access Lite (TACL) module.
 */
define('TACLE_RULES_TAC_LITE_NAME', 'Taxonomy Access Control Lite');

/**
 * Implements hook_rules_action_info().
 */
function tacle_rules_rules_action_info() {
  return array(
    'tacle_rules_action_single_term_role_permission' => array(
      'label' => t('Modify single term access for role(s)'),
      'arguments' => array(
        'taxonomy_term' => array(
          'type' => 'taxonomy_term',
          'label' => t('Taxonomy term'),
        ),
      ),
      'help' => t('Grant or revoke term access for role using %tac_lite.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME)),
      'module' => TACLE_RULES_TAC_LITE_NAME,
    ),
    'tacle_rules_action_multiple_term_role_permission' => array(
      'label' => t('Modify multiple term access for role(s)'),
      'help' => t('Grant or revoke multiple term access for role using %tac_lite.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME)),
      'module' => TACLE_RULES_TAC_LITE_NAME,
    ),
    'tacle_rules_action_single_term_user_permission' => array(
      'label' => t('Modify single term access for user'),
      'arguments' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('User'),
        ),
        'taxonomy_term' => array(
          'type' => 'taxonomy_term',
          'label' => t('Taxonomy term'),
        ),
      ),
      'help' => t('Grant or revoke term access for user using %tac_lite.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME)),
      'module' => TACLE_RULES_TAC_LITE_NAME,
    ),
    'tacle_rules_action_multiple_term_user_permission' => array(
      'label' => t('Modify multiple term access for user'),
      'arguments' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('User'),
        ),
      ),
      'help' => t('Grant or revoke multiple term access for user using %tac_lite.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME)),
      'module' => TACLE_RULES_TAC_LITE_NAME,
    ),
  );
}

/**
 * Implements hook_rules_condition_info().
 */
function tacle_rules_rules_condition_info() {
  return array(
    'tacle_rules_condition_is_tacl_term' => array(
      'label' => t('Check term is controlled by TACL'),
      'arguments' => array(
        'taxonomy_term' => array(
          'type' => 'taxonomy_term',
          'label' => t('Taxonomy term'),
        ),
      ),
      'module' => TACLE_RULES_TAC_LITE_NAME,
    ),
    'tacle_rules_condition_is_tacl_vocabulary' => array(
      'label' => t('Check vocabulary is controlled by TACL'),
      'arguments' => array(
        'taxonomy_vocabulary' => array(
          'type' => 'taxonomy_vocabulary',
          'label' => t('Taxonomy vocabulary'),
        ),
      ),
      'module' => TACLE_RULES_TAC_LITE_NAME,
    ),
    'tacle_rules_condition_user_has_tacl_access' => array(
      'label' => t('Check user account access rights to term(s)'),
      'arguments' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('User account'),
        ),
      ),
      'help' => t('Check if user account has access to TACL term(s) by role or
        specifically by account. If multiple terms or access types are selected
        then user must have access to all terms in all access types.'),
      'module' => TACLE_RULES_TAC_LITE_NAME,
    ),
  );
}

/**
 * Action: Modify single term access for role(s).
 *
 * @param object $taxonomy_term
 * @param array $settings
 */
function tacle_rules_action_single_term_role_permission($taxonomy_term, $settings) {
  $terms = array($taxonomy_term->tid => $taxonomy_term);
  tacle_rules_action_multiple_term_role_permission($terms, $settings);
}

/**
 * Action: Modify multiple term access for role(s).
 *
 * @param array $terms
 * @param array $settings
 */
function tacle_rules_action_multiple_term_role_permission($settings) {

  $schemes = _tacle_rules_valid_schemes($settings['tacle_rules']['schemes']);
  $terms = _tacle_rules_valid_terms($settings['tacle_rules']['terms']);
  $permission = _tacle_rules_valid_permission($settings['tacle_rules']['add_permission']);
  $roles = array_filter($settings['tacle_rules']['roles']);

  $vids = array();
  foreach ($schemes as $scheme_id) {
    $access = variable_get('tac_lite_grants_scheme_' . $scheme_id, array());
    foreach ($roles as $role) {
      switch ($permission) {
        case 'grant':
          foreach ($terms as $term) {
            if (!isset($access[$role][$term->vid][$term->tid])) {
              $access[$role][$term->vid][$term->tid] = $term->tid;
              $vids[$term->vid] = $term->vid;
            }
          }
          break;

        case 'revoke':
          foreach ($terms as $term) {
            if (isset($access[$role][$term->vid][$term->tid])) {
              unset($access[$role][$term->vid][$term->tid]);
              $vids[$term->vid] = $term->vid;
            }
          }
          break;
      }
    }

    if (count($vids)) {
      foreach ($roles as $role) {
        // Changes have been made. TAC uses a placeholder when no access has been
        // set. Scan through each vocabulary to see if we should add or remove the
        // placeholder. Finally save the role access.
        foreach ($vids as $vid) {
          if (empty($access[$role][$vid])) {
            $access[$role][$vid][''] = '';
          }
          else {
            unset($access[$role][$vid]['']);
          }
        }
      }
      variable_set('tac_lite_grants_scheme_' . $scheme_id, $access);
    }
  }
}

/**
 * Action: Modify single term access for a user.
 *
 * @param object $account
 * @param object $taxonomy_term
 * @param array $settings
 */
function tacle_rules_action_single_term_user_permission($account, $taxonomy_term, $settings) {
  $terms = array($taxonomy_term->tid => $taxonomy_term);
  tacle_rules_action_multiple_term_user_permission($account, $terms, $settings);
}

/**
 * Action: Modify multiple term access for a user.
 *
 * @param object $account
 * @param array $terms
 * @param array $settings
 */
function tacle_rules_action_multiple_term_user_permission($account, $settings) {

  $schemes = _tacle_rules_valid_schemes($settings['tacle_rules']['schemes']);
  $terms = _tacle_rules_valid_terms($settings['tacle_rules']['terms']);
  $permission = _tacle_rules_valid_permission($settings['tacle_rules']['add_permission']);

  foreach ($schemes as $id => $enabled) {
    $config = _tac_lite_config($id);
    $realm = $config['realm'];
    if (isset($account->$realm)) {
      $access = $account->$realm;
    }
    else {
      $access = array();
    }

    $vids = array();
    switch ($permission) {
      case 'grant':
        foreach ($terms as $term) {
          if (!isset($access[$term->vid][$term->tid])) {
            if (!isset($access[$term->vid])) {
              $access[$term->vid] = array();
            }
            $access[$term->vid][$term->tid] = $term->tid;
            $vids[$term->vid] = $term->vid;
          }
        }
        break;

      case 'revoke':
        foreach ($terms as $term) {
          if (isset($access[$term->vid][$term->tid])) {
            unset($access[$term->vid][$term->tid]);
            $vids[$term->vid] = $term->vid;
          }
        }
        break;
    }

    if (count($vids)) {
      // Changes have been made. TAC uses a placeholder when no access has been
      // set. Scan through each vocabulary to see if we should add or remove the
      // placeholder. Finally save the access rights to the account.
      foreach ($vids as $vid) {
        if (empty($access[$vid])) {
          $access[$vid][''] = '';
        }
        else {
          unset($access[$vid]['']);
        }
      }
      user_save($account, array($realm => $access), 'tac_lite');
    }
  }
}

/**
 * Condition: Check if vocabulary is controlled by TAC.
 *
 * @param object $taxonomy_vocabulary
 * @return bool
 */
function tacle_rules_condition_is_tacl_vocabulary($taxonomy_vocabulary, $settings) {
  $vids = variable_get('tac_lite_categories', array());
  return in_array($taxonomy_vocabulary->vid, $vids);
}

/**
 * Condition: Check if term is controlled by TAC.
 * 
 * @param object $taxonomy_term
 * @return bool
 */
function tacle_rules_condition_is_tacl_term($taxonomy_term, $settings) {
  $vids = variable_get('tac_lite_categories', array());
  return in_array($taxonomy_term->vid, $vids);
}

/**
 * Condition: Check if user account has access to TACL term.
 *
 * @param object $taxonomy_term
 * @param object $account
 * @param array $settings
 */
function tacle_rules_condition_user_has_tacl_access($account, $settings) {
  $terms = $settings['tacle_rules']['terms'];
  $ops = array_filter($settings['tacle_rules']['op']);
  $schemes = _tacle_rules_options_schemes();
  $schemes_loaded = array();
  foreach ($schemes as $id => $name) {
    $schemes_loaded[$id] = _tac_lite_config($id);
  }

  $access = TRUE;
  foreach ($ops as $op) {
    $grants = tac_lite_node_grants($account, $op);
    foreach ($schemes_loaded as $scheme) {
      foreach ($terms as $term) {
        if (!empty($scheme['perms']['grant_' . $op])) {
          if (empty($grants[$scheme['realm']][$term->tid])) {
            $access = FALSE;
          }
        }
      }
    }
  }
  return $access;
}

/**
 * Checks schemes variable to make sure it is valid.
 * @param array $schemes
 * @return
 *  Boolean FALSE on failure otherwise valid array schemes variable.
 */
function _tacle_rules_valid_schemes($schemes) {
  $schemes_all = _tacle_rules_options_schemes();
  if (empty($schemes_all)) {
    return FALSE;
  }

  $schemes_act = array_filter($schemes);
  if (empty($schemes_act)) {
    rules_log(t('%tac_lite scheme missing.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME)));
    return FALSE;
  }

  $schemes_valid = array();
  foreach ($schemes_act as $id => $enabled) {
    if (!isset($schemes_all[$id])) {
      rules_log(t('%tac_lite scheme no longer exists.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME)));
    }
    else {
      $schemes_valid[$id] = $enabled;
    }
  }
  return $schemes_valid;
}

/**
 * Checks terms to make sure they are valid.
 *
 * @param array $terms
 * @return
 *  Boolean FALSE on failure otherwise an array of valid terms.
 */
function _tacle_rules_valid_terms($terms) {
  $vids = _tacle_rules_options_vocabularies();
  if (empty($vids)) {
    return FALSE;
  }
  if (empty($terms)) {
    rules_log(t('TACLe Rules: No terms selected.'));
    return FALSE;
  }
  else {
    $terms_valid = array();
    foreach ($terms as $term) {
      if (empty($term->tid)) {
        rules_log(t('TACLe Rules: Term ID missing.'));
      }
      elseif (!in_array($term->vid, array_keys($vids))) {
        rules_log(t('TACLe Rules: Term ID !term_id not in %tac_lite controlled vocabulary.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME, '!term_id' => $term->vid)));
      }
      else {
        $terms_valid[] = $term;
      }
    }
    return $terms_valid;
  }
  return FALSE;
}

/**
 * Checks permission variable to make sure it is valid.
 *
 * @param string $permission
 * @return
 *  Boolean FALSE on failure otherwise valid string permission variable.
 */
function _tacle_rules_valid_permission($permission) {
  if (empty($permission)) {
    rules_log(t('TACLe Rules: Permission action missing'));
    return FALSE;
  }
  if (!in_array($permission, array('grant', 'revoke'))) {
    rules_log(t('TACLe Rules: Permission action not grant or revoke'));
    return FALSE;
  }
  return $permission;
}

// @todo: The functions would be better in the tac_lite module.

/**
 * Generate a list of vocabularies controlled by TACL. Return array keyed by
 * vocubulary ID with vocabulary name as value.
 *
 * @param bool $display_error
 * @return array
 */
function _tacle_rules_options_vocabularies($display_error = FALSE) {
  $vocabularies = taxonomy_get_vocabularies();
  $vids = variable_get('tac_lite_categories', array());
  $options = array();
  foreach ($vids as $vid) {
    $options[$vid] = $vocabularies[$vid]->name;
  }
  if (empty($options)) {
    $message = t('TACLe Rules: %tac_lite not configured to use any taxonomy vocabularies.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME));
    rules_log($message);
    if ($display_error) {
      drupal_set_message($message, 'error');
    }
  }
  return $options;
}

/**
 * Generate a list of TACL schemes keyed by scheme ID with scheme name as value.
 *
 * @param bool $display_error
 * @return array
 */
function _tacle_rules_options_schemes($display_error = FALSE) {
  $schemes = array();
  $schemes_max = variable_get('tac_lite_schemes', 1);
  for ($i = 1; $i <= $schemes_max; ++$i) {
    $config = _tac_lite_config($i);
    // A scheme without a name is not enabled.
    if (!empty($config['name'])) {
      $schemes[$i] = $config['name'];
    }
  }
  if (empty($schemes)) {
    $message = t('TACLe Rules: %tac_lite not configured to use any schemes.', array('%tac_lite' => TACLE_RULES_TAC_LITE_NAME));
    rules_log($message);
    if ($display_error) {
      drupal_set_message($message, 'error');
    }
  }
  return $schemes;
}
